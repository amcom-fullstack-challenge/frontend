import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Sales from "../pages/sales/ListSales"
import CreateSale from "../pages/sales/CreateSale"
import Commissions from "../pages/commissions/Commissions"

const RoutesComponent = () => {
  return (
    <Router>
      <Routes>
        <Route path="/sales" exact element={<Sales />} />
        <Route path="/create-sale" element={<CreateSale />} />
        <Route path="/commissions" element={<Commissions />} />
      </Routes>
    </Router>
  );
};

export default RoutesComponent;
