import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import SaleService from '../../services/sales';
import { useState } from 'react';
import { Calendar } from 'primereact/calendar';

const Commissions = () => {


  const [dates, setDates] = useState(null);
  const [commissions, setCommissions] = useState([]);


  const formatter = new Intl.NumberFormat('pt-BR', {
    style: 'currency',
    currency: 'BRL'
  });

  const generateCommissionList = (jsonData) => {

    if (jsonData.error || !jsonData.data) return [];

    const calculateCommission = (items) => {
      return items.reduce((totalCommission, item) => {
        return totalCommission + (parseFloat(item.price) * parseFloat(item.commission_percentage) / 100);
      }, 0);
    }

    let groupedBySeller = {};

    jsonData.data.forEach(invoice => {
      const commission = calculateCommission(invoice.items);
      const sellerId = invoice.seller.id;
      
      if (groupedBySeller[sellerId]) {
        groupedBySeller[sellerId].totalSales += parseFloat(invoice.total_amount);
        groupedBySeller[sellerId].totalCommissions += commission;
      } else {
        groupedBySeller[sellerId] = {
          sellerId: sellerId,
          seller: invoice.seller.name,
          totalSales: parseFloat(invoice.total_amount),
          totalCommissions: commission
        };
      }
    });

    return Object.values(groupedBySeller).map(sellerData => ({
      ...sellerData,
      totalSales: sellerData.totalSales.toFixed(2),
      totalCommissions: sellerData.totalCommissions.toFixed(2)
    }));

  };


  const handleRecoverCommissions = async () => {
    const response = await SaleService.listSales();
    const commissionList = generateCommissionList(response.data);
    setCommissions(commissionList);
    console.log(commissions);     
  }

  const totalSalesTemplate = (rowData) => {
    return formatter.format(rowData.totalSales);
  }

  const totalCommissionsTemplate = (rowData) => {
    return formatter.format(rowData.totalCommissions);
  }


  return (
    <div>
      <div className="d-flex justify-content-between m-2">
        <h1>Relatório de Comissões</h1>
        <div>
          <label className='mr-2' htmlFor="dateFilter">Data da busca:</label>
          <Calendar className='mr-4' inputId='dateFilter' value={dates} onChange={(e) => setDates(e.value)} selectionMode="range" readOnlyInput />
          <Button label="Buscar" onClick={handleRecoverCommissions} />
        </div>
      </div>
      <DataTable 
        value={commissions}
        dataKey="sellerId">

        <Column field="sellerId" header="Cód."></Column>
        <Column field="seller" header="Vendedor"></Column>
        <Column body={totalSalesTemplate} field="totalSales" header="Total de Vendas"></Column>
        <Column body={totalCommissionsTemplate} field="totalCommissions" header="Total de Comissões"></Column>
        
      </DataTable>
    </div>
  );
}

export default Commissions;
