import React, { useEffect, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { AutoComplete } from 'primereact/autocomplete';
import { InputNumber } from 'primereact/inputnumber';
import { Button } from 'primereact/button';
import { Dropdown } from 'primereact/dropdown';
import { Calendar } from 'primereact/calendar';
import SellerService from '../../services/sellers';
import CustomerService from '../../services/customers';
import ProductService from '../../services/products';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { ConfirmDialog, confirmDialog } from 'primereact/confirmdialog';
import SaleService from '../../services/sales';
import { Dialog } from 'primereact/dialog';

export const NovaVenda = () => {

  const [listProducts, setListProducts] = useState([]);
  
  return (
    <div className="container mt-5">
      <div className="row">
        <div className="col-12 col-md-8">
          <Produtos listProducts={listProducts} setListProducts={setListProducts} />
        </div>
        <div className="col-12 col-md-4" style={{ borderLeft: 'outset' }}>
          <DadosVenda listProducts={listProducts} setListProducts={setListProducts} />
        </div>
      </div>
    </div>
  );
}

const Produtos = ({ listProducts, setListProducts }) => {

  const [searchText, setSearchText] = useState("");
  const [filteredProducts, setFilteredProducts] = useState([]);
  const [products, setProducts] = useState([]);
  const [quantity, setQuantity] = useState(1);

  const formatter = new Intl.NumberFormat('pt-BR', {
    style: 'currency',
    currency: 'BRL'
  });

  const search = (event) => {
    const query = event.query.toLowerCase();
    const filtered = products
      .filter(
        (product) =>
          product.code.toLowerCase().includes(query) ||
          product.name.toLowerCase().includes(query)
      )
      .map((product) => ({
        label: `${product.code} - ${product.name}`,
        value: product,
      }));
      setFilteredProducts(filtered);
  };

  const addProduct = () => {
    const newProduct = {
      id: searchText.value.id,
      name: searchText.label,
      quantity: quantity,
      price: searchText.value.unit_price,
      unit_price: formatter.format(searchText.value.unit_price),
      total: formatter.format(searchText.value.unit_price * quantity),
    };
  
    const existingProductIndex = listProducts.findIndex(
      (product) => product.id === newProduct.id
    );
  
    if (existingProductIndex !== -1) {
      const updatedProducts = [...listProducts];
      const updatedQuantity = updatedProducts[existingProductIndex].quantity + quantity;
      updatedProducts[existingProductIndex].quantity = updatedQuantity;
      updatedProducts[existingProductIndex].total = formatter.format(searchText.value.unit_price * updatedQuantity)
  
      setListProducts(updatedProducts);
    } else {
      setListProducts(prevListProducts => [...prevListProducts, newProduct]);      
    }
  
    setSearchText("");
    setQuantity(1);
  };
  
  const deleteProduct = (product) => {
    const filtered = listProducts.filter((item) => item.id !== product.id);
    setListProducts(filtered);
  };

  const actionBodyTemplate = (rowData) => {
    return (
      <div>
        <Button icon="pi pi-trash" className="p-button-text p-button-danger" onClick={() => deleteProduct(rowData)} />
      </div>
    );
  }

  const handleRecoverProducts = async () => {
    const response = await ProductService.listProducts();
    setProducts(response.data.data);
  }

  useState(() => {
    handleRecoverProducts();
  }
  , []);


  return (
    <div>
      <h2>Produtos</h2>
      <div className="row">
        <div className='col-6'>
          <label htmlFor="itens-id" className="font-bold block mb-2">Busca pelo código de barras ou descrição</label>
          <AutoComplete className='d-grid' inputId='itens-id' value={searchText} field="label" suggestions={filteredProducts} completeMethod={search} onChange={(e) => setSearchText(e.value)} />
        </div>
        <div className='col-4'>
          <label htmlFor="qtd-itens-id" className="font-bold block mb-2">Quantidade de itens</label>
          <InputNumber className='d-grid' inputId="qtd-itens-id" value={quantity} onValueChange={(e) => setQuantity(e.value)} useGrouping={false} />
        </div>
        <div className='col-2 align-self-end'>
          <Button className='align-bottom' label="Adicionar" onClick={addProduct} />
        </div>
      </div>
      <div className='row mt-4'>
        <DataTable
          value={listProducts}
          dataKey="id">
          <Column field="name" header="Produtos/Serviços"></Column>
          <Column field="quantity" header="Quantidade"></Column>
          <Column field="unit_price" header="Preço unitário"></Column>
          <Column field="total" header="Total"></Column>
          <Column body={actionBodyTemplate} header="Ações"></Column>
        </DataTable>
      </div>
    </div>
  );
}

const DadosVenda = ({ listProducts, setListProducts }) => {

  const [saleCreatedAt, setSaleCreatedAt] = useState(new Date());
  const [selectedSeller, setSelectedSeller] = useState(null);
  const [sellers, setSellers] = useState([]);
  const [selectedCustomer, setSelectedCustomer] = useState(null);
  const [customers, setCustomers] = useState([]);
  const [total, setTotal] = useState(0);
  const [saleValid, setSaleValid] = useState(false);
  const [isSaleCreated, setIsSaleCreated] = useState(false);

  const formatter = new Intl.NumberFormat('pt-BR', {
    style: 'currency',
    currency: 'BRL'
  });

  useEffect(() => {
    setTotal(0);
    const handleTotal = () => {
      let total = listProducts.reduce((acc, product) => acc + parseFloat(product.price * product.quantity), 0);
      setTotal(total);
    }

    handleTotal();

    
  }, [listProducts]);


  const handleRecoverSellers = async () => {

    const response = await SellerService.listSellers();

    setSellers([
      ...response.data.data.map(seller => {
        return {
          ...seller,
          name: seller.name,
          code: seller.id
        }
      })
    ]);
    
  }

  const handleRecoverCustomers = async () => {
    
    const response = await CustomerService.listCustomers();

    setCustomers([
      ...response.data.data.map(customer => {
        return {
          ...customer,
          name: customer.name,
          code: customer.id
        }
      })
    ]);
    
  }

  const handleSendSale = async () => {
    const sale = {
      seller: selectedSeller.id,
      customer: selectedCustomer.id,
      total_amount: total,
      items: listProducts.map(product => {
        return {
          product: product.id,
          quantity: product.quantity,
          price: product.price
        }
      })
    };

    confirmDialog({
      message: 'Deseja finalizar a venda?',
      header: 'Finalizar Venda',
      icon: 'pi pi-exclamation-triangle',
      accept: async () => {
        const result = await SaleService.createSale(sale);
        if (result) {
          setListProducts([]);
          setSelectedSeller(null);
          setSelectedCustomer(null);
          setTotal(0);
          setIsSaleCreated(true);
        }
      },});

  }

  useEffect(() => {
    setSaleValid(
      selectedSeller != null &&
      selectedCustomer != null &&
      listProducts.length > 0
    );
  }
  , [selectedSeller, selectedCustomer, listProducts]);

  useState(() => {
    handleRecoverSellers();
    handleRecoverCustomers();
  }
  , []);

  const footerContent = (
    <div>
        <Button label="Ok" icon="pi pi-check" onClick={() => setIsSaleCreated(false)} autoFocus />
    </div>
);

  return (
    <div>

      <ConfirmDialog />
      <Dialog header="Sucesso" visible={isSaleCreated} onHide={() => setIsSaleCreated(false)} footer={footerContent}>
        <p className="m-0"> Venda realizada com sucesso! </p>
      </Dialog>

      <h2>Dados da venda</h2>
      <div className='row mb-4'>
        <label htmlFor="sale-created-id" className="font-bold block mb-2">Data e Hora da Venda</label>
        <Calendar 
          inputId='sale-created-id' 
          value={saleCreatedAt} 
          onChange={(e) => setSaleCreatedAt(e.value)}   
          showTime 
          hourFormat="24" 
          dateFormat="dd/mm/yy"
          disabled={true} />
      </div>
      <div className='row mb-4'>
        <label htmlFor="select-seller-id" className="font-bold block mb-2">Escolha um vendedor</label>
        <Dropdown value={selectedSeller} onChange={(e) => setSelectedSeller(e.value)} options={sellers} optionLabel="name" 
          inputId='select-seller-id'  editable placeholder="Escolha um vendedor" style={{ marginLeft: '10pt', maxWidth: '95%' }} />
      </div>
      <div className='row mb-4'>
        <label htmlFor="select-seller-id" className="font-bold block mb-2">Escolha um cliente</label>
        <Dropdown value={selectedCustomer} onChange={(e) => setSelectedCustomer(e.value)} options={customers} optionLabel="name" 
          inputId='select-seller-id'  editable placeholder="Escolha um cliente" style={{ marginLeft: '10pt', maxWidth: '95%' }} />
      </div>
      <div className='d-flex justify-content-between mt-4'>
        <strong>Valor total da venda: </strong>
        <h4>{formatter.format(total)}</h4>

      </div>
      <div className='d-flex justify-content-between mt-4'>
        <Button className='align-bottom' label="Cancelar"  />
        <Button className='align-bottom' label="Finalizar Venda" onClick={handleSendSale} disabled={!saleValid} />
      </div>
      
    </div>
  );
}

export default NovaVenda;
