import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import SaleService from '../../services/sales';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import moment from 'moment';
import _ from 'lodash';

const Sales = () => {

  const navigate = useNavigate();

  const [sales, setSales] = useState([]);

  const [expandedRows, setExpandedRows] = useState([]);

  const formatter = new Intl.NumberFormat('pt-BR', {
    style: 'currency',
    currency: 'BRL'
  });

  const onRowToggle = (event) => {
    setExpandedRows(event.data);
  }

  const toggleRow = (sale) => {
    let _expandedRows = {...expandedRows};
    if (_expandedRows[sale.invoice_number]) {
      delete _expandedRows[sale.invoice_number];
    } else {
      _expandedRows[sale.invoice_number] = true;
    }

    setExpandedRows(_expandedRows);
  }

  const rowExpansionTemplate = (data) => {

    const items = _.get(data, 'items').map(item => {

      const formattedAmount = formatter.format(item.price * item.quantity);
      
      return {
        ...item,
        formattedAmount: formattedAmount,
        commission_percentage: `${item.commission_percentage}%`,
        formattedcomission: formatter.format((item.price * item.quantity) * (item.commission_percentage / 100)),
        product_name: `${item.product.code} - ${item.product.name}`
      }
    });

    const subTotalItems = _.sumBy(items, item => parseFloat(item.price * item.quantity));
    const subTotalQuantity = _.sumBy(items, 'quantity');
    const subTotalCommission = _.sumBy(items, item => parseFloat((item.price * item.quantity) * (parseFloat(item.commission_percentage) / 100)));

    items.push({
      product_name: <strong>Total da Venda</strong>,
      quantity: <strong>{subTotalQuantity}</strong>,
      price: '',
      formattedAmount: <strong>{formatter.format(subTotalItems)}</strong>,
      commission_percentage: '',
      formattedcomission: <strong>{formatter.format(subTotalCommission)}</strong>
    });

    return (
      <div>
        <DataTable 
          value={items}>

          <Column field="product_name" header="Produto/Serviço"></Column>
          <Column field="quantity" header="Quantidade"></Column>
          <Column field="price" header="Preço unitário"></Column>
          <Column field="formattedAmount" header="Total do Produto"></Column>
          <Column field="commission_percentage" header="% de Comissão"></Column>
          <Column field="formattedcomission" header="Comissão"></Column>
        </DataTable>
      </div>
    );
  }

  const actionBodyTemplate = (rowData) => {
    return (
      <div>
        <Button icon="pi pi-search" className="p-button-text" onClick={() => toggleRow(rowData)} />
        <Button icon="pi pi-pencil" className="p-button-text p-button-warning" />
        <Button icon="pi pi-trash" className="p-button-text p-button-danger" />
      </div>
    );
  }

  const handleRecoverSales = () => {
    SaleService.listSales()
      .then(response => {
        setSales(
          _.get(response, 'data.data')
          .map(sale => {
            const formatter = new Intl.NumberFormat('pt-BR', {
                style: 'currency',
                currency: 'BRL'
            });
            const formattedAmount = formatter.format(sale.total_amount);
            return {
              ...sale,
              created: moment(sale.created).format('DD/MM/YYYY HH:mm:ss'),
              total_amount: formattedAmount
            }
          })
        );
      })
      .catch(error => {
        console.log(error);
      })
  }

  useEffect(() => {
    handleRecoverSales();
  }, []);

  return (
    <div>
      <div className="d-flex justify-content-between m-2">
        <h1>Vendas Realizadas</h1>
        <Button label="Inserir nova Venda" onClick={() => { navigate('/create-sale')}} />
      </div>
      <DataTable 
        value={sales} 
        rowExpansionTemplate={rowExpansionTemplate}
        expandedRows={expandedRows}
        onRowToggle={onRowToggle}
        dataKey="invoice_number">

        <Column field="invoice_number" header="Nota Fiscal"></Column>
        <Column field="customer.name" header="Cliente"></Column>
        <Column field="seller.name" header="Vendedor"></Column>
        <Column field="created" header="Data da Venda"></Column>
        <Column field="total_amount" header="Valor Total"></Column>
        <Column body={actionBodyTemplate} header="Opções"></Column>
        
      </DataTable>
    </div>
  );
}

export default Sales;
