import Api from "../api";

const ProductService = {
  listProducts: () => {
    return Api.get('/api/v1/products');
  },
}

export default ProductService;