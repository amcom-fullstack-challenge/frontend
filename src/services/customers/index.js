import Api from "../api";

const CostumerService = {
  listCustomers: () => {
    return Api.get('/api/v1/customers');
  },
}

export default CostumerService;