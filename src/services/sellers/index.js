import Api from "../api";

const SellerService = {
  listSellers: () => {
    return Api.get('/api/v1/sellers');
  },
}

export default SellerService;