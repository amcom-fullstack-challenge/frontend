import Api from "../api";

const SaleService = {
  listSales: () => {
    return Api.get('/api/v1/sales/?expand=customer,seller');
  },
  createSale: (data) => {
    return Api.post('/api/v1/sales/', data);
  },
}

export default SaleService;