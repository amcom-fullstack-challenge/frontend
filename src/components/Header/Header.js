import { useState } from "react";
import './styles.css';


const Sidebar = ({ isOpen, handleToggleSidebar }) => {
  return (
    <div className={`sidebar ${isOpen ? 'active' : ''}`}>
      <div className="sd-header">
        <h4 className="mb-0">Sidebar Header</h4>
        <div className="btn btn-primary" onClick={handleToggleSidebar}>
          <i className="fa fa-times"></i>
        </div>
      </div>
      <div className="sd-body">
        <ul>
          <li><a href="/sales" className="sd-link">Vendas</a></li>
          <li><a href="/commissions" className="sd-link">Comissões</a></li>
        </ul>
      </div>
    </div>
  );
}

const SidebarOverlay = ({ isOpen, handleToggleSidebar }) => {
  return (
    <div className={`sidebar-overlay ${isOpen ? 'active' : ''}`} onClick={handleToggleSidebar}></div>
  );
}

const Header = () => {

  const [isOpen, setIsOpen] = useState(false);

  const handleToggleSidebar = () => {
    setIsOpen(prevState => !prevState);
  }

  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-light bg-white shadow-md">
        <div className="container-fluid p-2">
          <a href="/#" className="navbar-brand text-primary mr-0"> Company Logo</a>
          <div className="form-inline ml-auto">
            <button className="btn btn-primary" onClick={handleToggleSidebar} >
              <i className="fa fa-bars"></i>
            </button>
          </div>
        </div>
      </nav>
      <Sidebar isOpen={isOpen} handleToggleSidebar={handleToggleSidebar} />
      <SidebarOverlay isOpen={isOpen} handleToggleSidebar={handleToggleSidebar} />
    </>
  )

}

export default Header;