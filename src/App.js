import React from 'react';
import './App.css';
import Header from './components/Header/Header';
import RoutesComponent from './routes';

function App() {
  return (
    <div>
      <Header />
      <div className='container-fluid mt-4'>
        <RoutesComponent />
      </div>
    </div>
  );
}

export default App;
